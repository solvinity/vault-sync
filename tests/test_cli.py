import os
from unittest.mock import patch

from click.testing import CliRunner

from vault_sync.cli import main


def test_list_keys(config_data, prepare_data):
    config_file = os.path.dirname(os.path.realpath(__file__)) + '/test_config_sync_once.json'

    runner = CliRunner()
    result = runner.invoke(main, ['--config', config_file, "--list-keys"])
    assert result.exit_code == 0
    assert "Secret keys in source:" in result.output


def test_sync_once_token(config_data, prepare_data):
    config_file = os.path.dirname(os.path.realpath(__file__)) + '/test_config_sync_once.json'

    runner = CliRunner()
    result = runner.invoke(main, ['--config', config_file])
    assert result.exit_code == 0


def test_sync_once_approle(config_data, testing_config):
    runner = CliRunner()
    result = runner.invoke(main, ['--config', testing_config])
    assert result.exit_code == 0


def test_sync_scheduled(config_data, prepare_data):
    config_file = os.path.dirname(os.path.realpath(__file__)) + '/test_config_scheduled.json'

    with patch('vault_sync.cli.loop') as loop_mocker:
        loop_mocker.side_effect = [True, True, True, False]

        runner = CliRunner()
        result = runner.invoke(main, ['--config', config_file])
        assert result.exit_code == 0

        # we expect the loop to be called 4 times
        assert loop_mocker.call_count == 4

