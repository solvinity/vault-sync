import json
import os
import subprocess
import tempfile
import time
import platform
from pathlib import Path
from zipfile import ZipFile

import hvac
import pytest
import requests

from .constants import INVALID_UUID, TOKEN, VALID_UUID

VAULT_VERSION_USED_IN_TEST = "1.18.3"

@pytest.fixture
def config_data():
    return {
        "source": {
            "url": "http://test1.com",
            "role_id": VALID_UUID,
            "secret_id": VALID_UUID,
            "kv_store": "store1",
        }, "destination": {
            "url": "http://test2.com",
            "role_id": VALID_UUID,
            "secret_id": VALID_UUID,
            "kv_store": "store2",
        }
    }


@pytest.fixture(scope="session")
def temp_dir():
    with tempfile.TemporaryDirectory() as temp_dir:
        yield temp_dir


@pytest.fixture
def config_file(temp_dir, config_data):
    config_file = Path(temp_dir).joinpath("config.json")
    with config_file.open("w") as fp:
        fp.write(json.dumps(config_data))
    return str(config_file)


@pytest.fixture
def invalid_json_config(temp_dir, config_data):
    config_file = Path(temp_dir).joinpath("config-json.json")
    data = json.dumps(config_data)
    with config_file.open("w") as fp:
        fp.write(f"{data},")
    return str(config_file)


@pytest.fixture
def invalid_value_config(temp_dir, config_data):
    config_file = Path(temp_dir).joinpath("config-data.json")
    config_data["source"]["secret_id"] = INVALID_UUID
    with config_file.open("w") as fp:
        fp.write(json.dumps(config_data))
    return str(config_file)


@pytest.fixture(scope="module")
def vault_engines(temp_dir):
    """Prepare and initialise two vault instances in-memory
    """

    operating_system = platform.system().lower()

    match platform.machine():
        case "x86_64":
            machine = "amd64"
        case "arm64":
            machine = "arm64"
        case _:
            raise ValueError(f"Unsupported machine type: {platform.machine()}")

    vault_url = (
        "https://releases.hashicorp.com/vault/"
        f"{VAULT_VERSION_USED_IN_TEST}"
        f"/vault_{VAULT_VERSION_USED_IN_TEST}_{operating_system}_{machine}.zip"
    )
    vault_zip = Path("/tmp", "vault.zip")
    if not vault_zip.exists():
        resp = requests.get(vault_url, allow_redirects=True)

        with vault_zip.open("wb") as fp:
            fp.write(resp.content)

    vault_file = Path("/tmp", "vault")
    if not vault_file.exists():
        with ZipFile(vault_zip, "r") as archive:
            archive.extractall("/tmp")

        # Resulting file is named 'vault'
        os.chmod(vault_file, 0o755)

    vault = subprocess.Popen(
        [
            "/tmp/vault", "server", "-dev",
            "-dev-root-token-id", "aabbccddeeff"
        ],
        stdout=subprocess.DEVNULL,
        stderr=subprocess.DEVNULL,
    )

    time.sleep(5)

    # prepare vault with auth approle and retrieve ids.
    data = {"source": {}, "destination": {}}
    client = hvac.Client(url="http://127.0.0.1:8200", token=TOKEN)

    client.sys.enable_secrets_engine(backend_type="kv", path="sync1", options={"version": "2"})
    client.sys.enable_secrets_engine(backend_type="kv", path="sync2", options={"version": "2"})

    # Prepare reading policy
    client.sys.create_or_update_policy(
        name="read-policy",
        policy={
            "path": {
                "sync1/data/*": {"capabilities": ["read", "list"]},
                "sync1/metadata/*": {"capabilities": ["read", "list"]},
                "sync1/*": {"capabilities": ["read", "list"]},
                "sync1": {"capabilities": ["read", "list"]},
            }
        }
    )

    client.sys.enable_auth_method(method_type="approle")

    client.auth.approle.create_or_update_approle(role_name="test-one", token_policies=["read-policy"])
    client.secrets.kv.v2.configure(max_versions=5, mount_point="sync1")

    resp = client.auth.approle.read_role_id(role_name="test-one")
    data["source"]["role_id"] = resp["data"]["role_id"]
    resp = client.auth.approle.generate_secret_id(role_name="test-one")
    data["source"]["secret_id"] = resp["data"]["secret_id"]

    _ = client.secrets.kv.v2.create_or_update_secret(
        path="test", secret={"test": "test"}, mount_point="sync1"
    )
    _ = client.secrets.kv.v2.create_or_update_secret(
        path="nested/test", secret={"test": "test"}, mount_point="sync1"
    )
    _ = client.secrets.kv.v2.create_or_update_secret(
        path="doublenested/doublenested/test", secret={"test": "test"}, mount_point="sync1"
    )
    _ = client.secrets.kv.v2.create_or_update_secret(
        path="orphaned", secret={"test": "test"}, mount_point="sync2"
    )

    # Prepare reading policy
    client.sys.create_or_update_policy(
        name="create-policy",
        policy={
            "path": {
                "sync2/data/*": {"capabilities": ["create", "read", "update", "list"]},
                "sync2/*": {"capabilities": ["create", "read", "update", "list"]}
            }
        }
    )

    client.auth.approle.create_or_update_approle(role_name="test-two", token_policies=["create-policy"])
    client.secrets.kv.v2.configure(max_versions=5, mount_point="sync2")
    resp = client.auth.approle.read_role_id(role_name="test-two")
    data["destination"]["role_id"] = resp["data"]["role_id"]
    resp = client.auth.approle.generate_secret_id(role_name="test-two")
    data["destination"]["secret_id"] = resp["data"]["secret_id"]

    _ = client.secrets.kv.v2.create_or_update_secret(
        path="test", secret={"test": "old"}, mount_point="sync2"
    )

    try:
        yield vault, data
    finally:
        vault.kill()
        time.sleep(3)


@pytest.fixture(scope="module")
def prepare_data(vault_engines):
    _, uuids = vault_engines

    data = {
        "source": {"url": "http://127.0.0.1:8200", "kv_store": "sync1"},
        "destination": {"url": "http://127.0.0.1:8200", "kv_store": "sync2"}
    }
    data["source"].update(uuids["source"])
    data["destination"].update(uuids["destination"])

    yield data


@pytest.fixture(scope="module")
def testing_config(prepare_data, temp_dir):
    """Prepare, write and return a config file for two vault instances."""
    config_file = Path(temp_dir, "testing_config.json")
    with config_file.open("w") as fp:
        fp.write(json.dumps(prepare_data))
    return str(config_file)
