from unittest.mock import Mock, patch

import pytest
from loguru import logger
from vault_sync.exceptions import ConfigException
from vault_sync.vault_sync import VaultSync

from .constants import VALID_UUID

"""
For the structure of the vault, assume a very simple one:

- test_one
- single/test
- double/double/test
- test_two

"""

ROOT_KEYS = ["test_one", "single/", "double/", "test_two"]
SINGLE_KEYS = ["test"]
DOUBLE_KEY_ONE = ["double/"]
DOUBLE_KEY_TWO = ["test"]

logger.remove()


@pytest.fixture
@patch("hvac.Client")
def mocked_sync(mocked_hvac, config_file):
    """Given we cannot test without functional vault engines, create a mock"""
    VaultSync.authenticate_clients = Mock()
    return VaultSync(config_file)


def test_vault_sync_config_valid(config_file):
    config = VaultSync.load_config(config_file=config_file)
    assert config.source.role_id == VALID_UUID


@pytest.mark.parametrize("config", ["invalid_json_config", "invalid_value_config"])
def test_vault_sync_config_invalid(config, request):
    """Both issues with JSON and pydantic validation will raise the same exception"""
    config = request.getfixturevalue(config)
    with pytest.raises(ConfigException):
        VaultSync.load_config(config_file=config)


def test_vault_sync_list_folders(mocked_sync):
    mocked_sync.source_client.secrets.kv.v2.list_secrets = Mock(
        return_value={"data": {"keys": ROOT_KEYS}}
    )
    folders = mocked_sync.list_folders(mocked_sync.source_client, "", mount_point=mocked_sync.config.source.kv_store)
    assert folders == ["single/", "double/"]


def test_vault_sync_list_keys(mocked_sync):
    mocked_sync.source_client.secrets.kv.v2.list_secrets = Mock(
        return_value={"data": {"keys": ROOT_KEYS}}
    )
    folders = mocked_sync.list_keys(mocked_sync.source_client, "", mount_point=mocked_sync.config.source.kv_store)
    assert folders == ["test_one", "test_two"]


def test_vault_sync_list_all_paths_in_source(mocked_sync):
    mocked_sync.source_client.secrets.kv.v2.list_secrets = Mock(
        side_effect=[
            {"data": {"keys": ROOT_KEYS}},
            {"data": {"keys": SINGLE_KEYS}},
            {"data": {"keys": DOUBLE_KEY_ONE}},
            {"data": {"keys": DOUBLE_KEY_TWO}}
        ]
    )
    folders = mocked_sync.list_all_paths(client=mocked_sync.source_client, mount_point=mocked_sync.config.source.kv_store)
    assert folders == ["", "single/", "double/", "double/double/"]


def test_vault_sync_list_all_keys_in_source(mocked_sync):
    mocked_sync.source_client.secrets.kv.v2.list_secrets = Mock(
        side_effect=[
            {"data": {"keys": ROOT_KEYS}},
            {"data": {"keys": SINGLE_KEYS}},
            {"data": {"keys": DOUBLE_KEY_ONE}},
            {"data": {"keys": DOUBLE_KEY_TWO}},
            {"data": {"keys": ROOT_KEYS}},
            {"data": {"keys": SINGLE_KEYS}},
            {"data": {"keys": DOUBLE_KEY_ONE}},
            {"data": {"keys": DOUBLE_KEY_TWO}}
        ]
    )
    folders = mocked_sync.list_all_keys(client=mocked_sync.source_client, mount_point=mocked_sync.config.source.kv_store)
    assert folders == ["test_one", "test_two", "single/test", "double/double/test"]


def test_vault_sync_authenticate(testing_config):
    sync = VaultSync(config_file=testing_config)

    sync.authenticate_clients()

    assert sync.source_client.is_authenticated()


def test_vault_sync_secret_from_source(testing_config):
    sync = VaultSync(config_file=testing_config)

    result = sync.get_secret(sync.source_client, secret_path="test", mount_point=sync.config.source.kv_store)
    assert result == {"test": "test"}


def test_vault_sync_secret_from_source_incorrect(testing_config):
    sync = VaultSync(config_file=testing_config)

    assert sync.get_secret(sync.source_client, secret_path="nonexistent", mount_point=sync.config.source.kv_store) is None


def test_vault_sync_all(testing_config):
    sync = VaultSync(config_file=testing_config)

    # Check that the destination only has 1 secret.
    list_response = sync.destination_client.secrets.kv.v2.list_secrets(
        path="", mount_point=sync.config.destination.kv_store,
    )
    result = list_response["data"]["keys"]

    assert len(result) == 2
    for keys in ["test", "orphaned"]:
        assert keys in result

    # Now sync all secrets
    sync.sync_all()

    # And check that all secrets have been copied over
    list_response = sync.destination_client.secrets.kv.v2.list_secrets(
        path="", mount_point=sync.config.destination.kv_store,
    )
    result = list_response["data"]["keys"]

    assert len(result) == 3
    assert all(x in ["test", "nested/", "doublenested/"] for x in result)
