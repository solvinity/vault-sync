from copy import deepcopy

import pydantic
import pytest

from vault_sync.config import Config, is_valid_uuid

from .constants import BROKEN_UUID, INVALID_UUID, VALID_UUID


def test_is_valid_uuid():
    result = is_valid_uuid(VALID_UUID)
    assert result is True


def test_is_invalid_uuid():
    result = is_valid_uuid(INVALID_UUID)
    assert result is False


def test_is_incorrect_uuid():
    result = is_valid_uuid(BROKEN_UUID)
    assert result is False


def test_config_is_valid(config_data):
    config = Config(**config_data)
    assert config.destination.secret_id == VALID_UUID


@pytest.mark.parametrize("auth_method", ["token", "approle", "kubernetes"])
def test_config_auth_method(config_data, auth_method):
    test_config = deepcopy(config_data)

    for key in ["source", "destination"]:
        test_config[key]["auth_method"] = auth_method

        if auth_method == "token":
            del test_config[key]["role_id"]
            del test_config[key]["secret_id"]
            test_config[key]["token"] = VALID_UUID
        elif auth_method == "kubernetes":
            del test_config[key]["role_id"]
            del test_config[key]["secret_id"]
            test_config[key]["token_path"] = "/run/secrets/kubernetes.io/serviceaccount/token"
            test_config[key]["role_name"] = "vrl-vault-sync"

        Config(**test_config)

def test_config_incorrect_uuid(config_data):
    config_data["source"]["role_id"] = INVALID_UUID

    with pytest.raises(pydantic.ValidationError):
        Config(**config_data)
